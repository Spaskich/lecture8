#include <iostream>
using namespace std;



double add (double a, double b){
    double result = a + b;
    return result;
}

double substract (double a, double b){
    double result = a - b;
    return result;
}

double multiply (double a, double b){
    double result = a * b;
    return result;
}

double divide (double a, double b){
    double result = a / b;
    return result;
}

double call(double a, double b, char operand){
    int output;
    if (operand == '+') output = add(a,b);
    else if (operand == '-') output = substract(a,b);
    else if (operand == '*') output = multiply(a,b);
    else if (operand == '/') output = divide(a,b);
    return output;
}

int main(){
    int a, b;
    char operand;
    cout << "Enter operation: ";
    cin >> a >> operand >> b;

    cout << call(a,b,operand);

    return 0;
}
